﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Kore;
using Kore.Controllers;
using Kore.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace KoreTests {

    public class CabControllerTests
    {
        private readonly Mock<ICabRepository> _mockRepo;
        private readonly Mock<ILogger<CabController>> _mockLogger;
        private readonly CabController _controller;
        private Mock<IHttpContextAccessor> _mockHttpContextAccessor;

        public CabControllerTests() {
            _mockRepo = new Mock<ICabRepository>();
            _mockLogger = new Mock<ILogger<CabController>>();
            _mockHttpContextAccessor = new Mock<IHttpContextAccessor>();
            _controller = new CabController(_mockLogger.Object, _mockRepo.Object, 
                                            _mockHttpContextAccessor.Object);
        }

        [Fact]
        public void GetShouldNotBeNull()
        {
            Cab cab = new Cab();
            cab.Id = 1234;
            cab.Name = "cab0";
            _mockRepo.Setup(repo => repo.GetCab(0)).Returns(cab);
            var result = _controller.Get(0);
            Assert.IsType<Cab>(result);
            Assert.Equal(1234, result.Id);
        }

           [Fact]
           public void PutAsyncShouldNotBeNull()
           {
               Task<Cab> cab2 = new(() => {
                   Cab c = new Cab();
                   c.Id = 1234;
                   c.Name = "cab0";
                   return c;
               });
               _mockRepo.Setup(repo => repo.ModifyCabAsync(It.IsAny<Cab>())).Returns(cab2);

               /*var context = new DefaultHttpContext();
               context.Request.Headers["Authorization"] = "Y2FiMDpjYWIw"; // cab0:cab0
               _mockHttpContextAccessor.Setup(_ => _.HttpContext).Returns(context);
               */

               _mockHttpContextAccessor.Setup(o => o.HttpContext.User.Claims).Returns(
                       new[] { new Claim("userid", "1234") }
                   );

               Cab cab = new Cab();
               cab.Id = 1234;
               cab.Name = "cab0";
               /*ActionResult<Cab> result = await _controller.Put("1234", cab);
               Cab res = result.Value;*/
                var task= _controller.Put();
                task.Wait();
                Cab res = task.Result as Cab;
               Assert.Equal(1234, res.Id);
           }
/*
        [Fact]
        public void PutShouldNotBeNull()
        {
            Cab c = new Cab();
            c.Id = 1234;
            c.Name = "cab0";
            _mockRepo.Setup(repo => repo.ModifyCab(It.IsAny<Cab>())).Returns(c);

            _mockHttpContextAccessor.Setup(o => o.HttpContext.User.Claims).Returns(
                    new[] { new Claim("userid", "1234") }
                );

            Cab cab = new Cab();
            cab.Id = 1234;
            cab.Name = "cab0";
            ActionResult<Cab> result = _controller.Put("1234", cab);
            Cab res = result.Value;
            Assert.Equal(1234, res.Id);
        }*/
    }
}
